﻿using Hahn.ApplicationProcess.December2020.Data.Context;
using Hahn.ApplicationProcess.December2020.Domain.Interfaces;
using Hahn.ApplicationProcess.December2020.Domain.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Hahn.ApplicationProcess.December2020.Data.Repositories
{
	/// <summary>
	/// Applicant repository
	/// </summary>
	public class ApplicantRepository : IApplicantRepository
	{
		private readonly ApplicationProcessDbContext _context;

		/// <summary>
		/// Applicant repository constructor
		/// </summary>
		public ApplicantRepository(ApplicationProcessDbContext context)
		{
			_context = context;
		}

		///// <summary>
		///// Applicant repository destructor
		///// </summary>
		//public void Dispose()
		//{
		//	_context.Dispose();

		//	GC.SuppressFinalize(this);
		//}

		/// <summary>
		/// Retrieves all applicants
		/// </summary>
		/// <param name="cancellationToken">The cancellation token</param>
		/// <returns>The applicants list</returns>
		public async Task<IEnumerable<Applicant>> GetApplicantsAsync(CancellationToken cancellationToken = default)
		{
			return await _context.Applicants.ToListAsync(cancellationToken);
		}

		/// <summary>
		/// Retrieves an applicant by the id
		/// </summary>
		/// <param name="id" example="1">The applicant id</param>
		/// <param name="cancellationToken">The cancellation token</param>
		/// <returns>The applicant</returns>
		public async Task<Applicant> GetApplicantAsync(int id, CancellationToken cancellationToken = default)
		{
			return await _context.Applicants.FirstOrDefaultAsync(a => a.Id == id, cancellationToken);
		}

		/// <summary>
		/// Inserts a new applicant
		/// </summary>
		/// <param name="applicant">The applicant edit model</param>
		/// <param name="cancellationToken">The cancellation token</param>
		/// <returns>The applicant id</returns>
		public async Task<int> InsertApplicantAsync(Applicant applicant, CancellationToken cancellationToken = default)
		{
			_context.Applicants.Add(applicant);
			await _context.SaveChangesAsync(cancellationToken);

			return applicant.Id;
		}

		/// <summary>
		/// Updates an applicant
		/// </summary>
		/// <param name="id" example="1">The applicant unique identifier</param>
		/// <param name="applicant">The applicant edit model</param>
		/// <param name="cancellationToken">The cancellation token</param>
		/// <returns>False if applicant not found</returns>
		public async Task<bool> UpdateApplicantAsync(Applicant applicant, CancellationToken cancellationToken = default)
		{
			bool ret = false;

			var item = await _context.Applicants.FirstOrDefaultAsync(a => a.Id == applicant.Id, cancellationToken);
			if (item != null)
			{
				_context.Entry(item).CurrentValues.SetValues(applicant);

				await _context.SaveChangesAsync(cancellationToken);

				ret = true;
			}

			return ret;
		}

		/// <summary>
		/// Deletes an applicant
		/// </summary>
		/// <param name="id" example="1">The applicant unique identifier</param>
		/// <param name="cancellationToken">The cancellation token</param>
		/// <returns>False if applicant not found</returns>
		public async Task<bool> DeleteApplicantAsync(int id, CancellationToken cancellationToken = default)
		{
			bool ret = false;

			var item = await _context.Applicants.FirstOrDefaultAsync(a => a.Id == id, cancellationToken);
			if (item != null)
			{
				_context.Applicants.Remove(item);

				await _context.SaveChangesAsync(cancellationToken);

				ret = true;
			}

			return ret;
		}
	}
}
