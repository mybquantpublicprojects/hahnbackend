﻿using Hahn.ApplicationProcess.December2020.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace Hahn.ApplicationProcess.December2020.Data.Context
{
	public class ApplicationProcessDbContext : DbContext
	{
		public ApplicationProcessDbContext(DbContextOptions<ApplicationProcessDbContext> options)
		: base(options)
		{
		}

		public DbSet<Applicant> Applicants { get; set; }
	}
}
