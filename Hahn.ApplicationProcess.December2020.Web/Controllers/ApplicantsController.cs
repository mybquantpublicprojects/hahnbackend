﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using Hahn.ApplicationProcess.December2020.Domain.Models;
using Hahn.ApplicationProcess.December2020.Web.Services;
using Hahn.ApplicationProcess.December2020.Web.Models;
using Microsoft.AspNetCore.Cors;
using System.Threading;

namespace Hahn.ApplicationProcess.December2020.Web.Controllers
{
	/// <summary>
	/// Applicants api controller
	/// </summary>
	[EnableCors("DefaultPolicy")]
	[ApiController]
	[Route("api/[controller]")]
	public class ApplicantsController : ControllerBase
	{
		private readonly ILogger<ApplicantsController> _logger;
		private readonly IApplicantService _applicantService;
		private readonly IApplicantValidatorCacheService _applicantValidatorCacheService;

		/// <summary>
		/// Applicants controller constructor
		/// </summary>
		public ApplicantsController(ILogger<ApplicantsController> logger, IApplicantService applicantService, IApplicantValidatorCacheService applicantValidatorCacheService)
		{
			_logger = logger;
			_applicantService = applicantService;
			_applicantValidatorCacheService = applicantValidatorCacheService;
		}

		/// <summary>
		/// Retrieves all applicants
		/// </summary>
		/// <response code="200">Success</response>
		/// <response code="500">Oops! Can't get applicants right now</response>
		[HttpGet]
		[ProducesResponseType(typeof(IEnumerable<Applicant>), StatusCodes.Status200OK)]
		[ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
		public async Task<IActionResult> Get()
		{
			try
			{
				return Ok(await _applicantService.GetApplicantsAsync(HttpContext.RequestAborted));
			}
			catch (Exception ex)
			{
				_logger.LogError(ex, string.Format("Get: {0}", ex.Message));

				return Problem(ex.Message);
			}
		}

		/// <summary>
		/// Retrieves a specific applicant by unique id
		/// </summary>
		/// <param name="id" example="1">The applicant id</param>
		/// <response code="200">Success</response>
		/// <response code="404">Applicant not found</response>
		/// <response code="500">Oops! Can't get your applicant right now</response>
		[HttpGet("{id}", Name = "GetApplicantById")]
		[ProducesResponseType(typeof(Applicant), StatusCodes.Status200OK)]
		[ProducesResponseType(StatusCodes.Status404NotFound)]
		[ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
		public async Task<IActionResult> Get(int id)
		{
			try
			{
				Applicant applicant = await _applicantService.GetApplicantAsync(id, HttpContext.RequestAborted);

				// Applicant not found
				if (applicant == null)
				{
					return NotFound();
				}

				return Ok(applicant);
			}
			catch (Exception ex)
			{
				_logger.LogError(ex, "Get({0}): {1}", id, ex.Message);

				return Problem(ex.Message);
			}
		}

		/// <summary>
		/// Inserts a new applicant
		/// </summary>
		/// <param name="model">The applicant edit model</param>
		/// <response code="201" links="GetApplicantById">Applicant created</response>
		/// <response code="400">Applicant has missing/invalid values</response>
		/// <response code="500">Oops! Can't create your applicant right now</response>
		[HttpPost]
		[ProducesResponseType(StatusCodes.Status201Created)]
		[ProducesResponseType(typeof(ValidationProblemDetails), StatusCodes.Status400BadRequest)]
		[ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
		public async Task<IActionResult> Post([FromBody] ApplicantEditModel model)
		{
			try
			{
				// FluentValidation already manages ModelState

				int id = await _applicantService.InsertApplicantAsync(model, HttpContext.RequestAborted);

				return CreatedAtRoute("GetApplicantById", new { id }, null);
			}
			catch (Exception ex)
			{
				_logger.LogError(ex, "Post: {0}", ex.Message);

				return Problem(ex.Message);
			}
		}

		/// <summary>
		/// Updates an applicant
		/// </summary>
		/// <param name="id" example="1">The applicant id</param>
		/// <param name="model">The applicant edit model</param>
		/// <response code="204">Applicant modified</response>
		/// <response code="400">Applicant has missing/invalid values</response>
		/// <response code="404">Applicant not found</response>
		/// <response code="500">Oops! Can't modify your applicant right now</response>
		[HttpPut("{id}")]
		[ProducesResponseType(StatusCodes.Status204NoContent)]
		[ProducesResponseType(typeof(ValidationProblemDetails), StatusCodes.Status400BadRequest)]
		[ProducesResponseType(StatusCodes.Status404NotFound)]
		[ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
		public async Task<IActionResult> Put(int id, [FromBody] ApplicantEditModel model)
		{
			try
			{
				// FluentValidation already manages ModelState

				if (await _applicantService.UpdateApplicantAsync(id, model, HttpContext.RequestAborted))
				{
					return NoContent();
				}
				else
				{
					return NotFound();
				}
			}
			catch (Exception ex)
			{
				_logger.LogError(ex, "Put({0}): {1}", id, ex.Message);

				return Problem(ex.Message);
			}
		}

		/// <summary>
		/// Deletes an applicant
		/// </summary>
		/// <param name="id" example="1">The applicant id</param>
		/// <response code="204">Applicant deleted</response>
		/// <response code="404">Applicant not found</response>
		/// <response code="500">Oops! Can't delete your applicant right now</response>
		[HttpDelete("{id}")]
		[ProducesResponseType(StatusCodes.Status204NoContent)]
		[ProducesResponseType(StatusCodes.Status404NotFound)]
		[ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
		public async Task<IActionResult> Delete(int id)
		{
			try
			{
				if (await _applicantService.DeleteApplicantAsync(id, HttpContext.RequestAborted))
				{
					return NoContent();
				}
				else
				{
					return NotFound();
				}
			}
			catch (Exception ex)
			{
				_logger.LogError(ex, "Delete({0}): {1}", id, ex.Message);

				return Problem(ex.Message);
			}
		}

		/// <summary>
		/// Validates an applicant field
		/// </summary>
		/// <param name="id" example="name">The applicant field name to validate</param>
		/// <param name="value" example="Massimo">The applicant field value</param>
		/// <response code="200">Applicant deleted</response>
		/// <response code="400">Applicant not found</response>
		/// <response code="500">Oops! Can't delete your applicant right now</response>
		[HttpGet("validate/{id}")]
		[ProducesResponseType(StatusCodes.Status200OK)]
		[ProducesResponseType(typeof(ValidationProblemDetails), StatusCodes.Status400BadRequest)]
		[ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
		public async Task<IActionResult> Validate(string id, string value)
		{
			try
			{
				bool valid = id switch
				{
					"country" => await _applicantValidatorCacheService.IsValidCountryAsync(value, HttpContext.RequestAborted),
					"email" => await _applicantValidatorCacheService.IsValidEmailAddressAsync(value, HttpContext.RequestAborted),
					_ => throw new Exception("invalid field name"),
				};
				if (valid)
				{
					return Ok();
				}
				else
				{
					return BadRequest();
				}
			}
			catch (Exception ex)
			{
				_logger.LogError(ex, "Validate('{0}','{1}'): {2}", id, value, ex.Message);

				return Problem(ex.Message);
			}
		}
	}
}
