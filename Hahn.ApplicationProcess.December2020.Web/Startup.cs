using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using System.Text.Json;
using FluentValidation.AspNetCore;
using FluentValidation;
using Swashbuckle.AspNetCore.Swagger;
using System.Globalization;
using System.Collections.Generic;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Hahn.ApplicationProcess.December2020.Web.Models;
using Hahn.ApplicationProcess.December2020.Web.Services;
using Hahn.ApplicationProcess.December2020.Domain.Interfaces;
using Hahn.ApplicationProcess.December2020.Data.Repositories;
using Hahn.ApplicationProcess.December2020.Data.Context;
using Hahn.ApplicationProcess.December2020.Web.Validators;

namespace Hahn.ApplicationProcess.December2020.Web
{
	/// <summary>
	/// The Startup class
	/// </summary>
	public class Startup
	{
		/// <summary>
		/// The Startup constructor
		/// </summary>
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		/// <summary>
		/// Configuration
		/// </summary>
		public IConfiguration Configuration { get; }

		/// <summary>
		/// This method gets called by the runtime. Use this method to add services to the container.
		/// </summary>
		public void ConfigureServices(IServiceCollection services)
		{
			// Use IHttpClientFactory to implement resilient HTTP requests
			services.AddHttpClient();

			// Register cors default policy
			services.AddCors(options =>
			{
				options.AddPolicy(name: "DefaultPolicy",
					builder =>
					{
						builder
							.WithOrigins(Configuration.GetSection("Cors:DefaultPolicy:Origins").Get<string[]>())
							.WithHeaders(new string[] { "content-type" })
							.WithMethods("GET", "POST", "PUT", "DELETE");
					});
			});

			// Json localization
			// Important: AddJsonLocalization must be called before AddViewLocalization.
			// Otherwise, view localization will just ignore the JSON localizer and use the default .resx localizer instead.
			services.AddJsonLocalization(services => services.ResourcesPath = "Localization");

			// Lowercase api endpoints
			services.Configure<RouteOptions>(options => options.LowercaseUrls = true);

			// Json serialization options and fluent validation
			services.AddMvc()
				.AddJsonOptions(options =>
				{
					options.JsonSerializerOptions.IgnoreNullValues = true;
					options.JsonSerializerOptions.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;
					options.JsonSerializerOptions.Converters.Add(new System.Text.Json.Serialization.JsonStringEnumMemberConverter());
				})
				.AddFluentValidation();

			// Register ApplicantRepository
			services.AddDbContext<ApplicationProcessDbContext>(options =>
			{
				options.UseInMemoryDatabase(Configuration.GetConnectionString("database"));
			});
			services.AddScoped<IApplicantRepository, ApplicantRepository>();
			//services.AddScoped<IApplicantRepository>(factory => new ApplicantRepository(Configuration.GetConnectionString("database")));

			// Register ApplicantService
			services.AddScoped<IApplicantService, ApplicantService>();

			// Register Applicant Validator Cache service
			services.AddSingleton<IApplicantValidatorCacheService, ApplicantValidatorCacheService>();

			// Register Applicant Validator
			services.AddTransient<IValidator<ApplicantEditModel>, ApplicantValidator>();

			// Register the Swagger generator, defining 1 or more Swagger documents
			services.AddSwaggerGen(options =>
			{
				options.SwaggerDoc("v1", new OpenApiInfo { Title = "Web API", Version = "v1" });
				options.IncludeXmlComments(System.IO.Path.Combine(System.AppContext.BaseDirectory, "Hahn.ApplicationProcess.December2020.Domain.xml"));
				options.IncludeXmlComments(System.IO.Path.Combine(System.AppContext.BaseDirectory, "Hahn.ApplicationProcess.December2020.Web.xml"));
				options.AddFluentValidationRules();
			});

			services.AddControllers();
		}

		/// <summary>
		/// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		/// </summary>
		public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}
			else
			{
				app.UseHttpsRedirection();
			}

			app.UseRouting();

			app.UseAuthorization();

			app.UseCors();

			// Localization
			var supportedCultures = new List<CultureInfo>
			{
				new CultureInfo("en"),
				new CultureInfo("de"),
				new CultureInfo("it"),
			};
			var options = new RequestLocalizationOptions
			{
				DefaultRequestCulture = new RequestCulture("en"),
				SupportedCultures = supportedCultures,
				SupportedUICultures = supportedCultures
			};
			app.UseRequestLocalization(options);

			// Enable middleware to serve generated Swagger as a JSON endpoint.
			app.UseSwagger();

			// Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
			// specifying the Swagger JSON endpoint.
			app.UseSwaggerUI(options =>
			{
				options.SwaggerEndpoint("/swagger/v1/swagger.json", "Web API V1");
			});

			app.UseEndpoints(endpoints =>
			{
				endpoints.MapControllers();
			});
		}
	}
}
