﻿using Microsoft.Extensions.Configuration;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.RegularExpressions;
using Ryadel.Components.Threading;
using System;
using Microsoft.Extensions.Logging;

namespace Hahn.ApplicationProcess.December2020.Web.Services
{
	/// <summary>
	/// Manages applicant validator cache
	/// </summary>
	public interface IApplicantValidatorCacheService
	{
		/// <summary>
		/// Checks if a country is valid
		/// </summary>
		/// <param name="country">The country name</param>
		/// <param name="cancellationToken">The cancellation token</param>
		/// <returns>True if country is present</returns>
		Task<bool> IsValidCountryAsync(string country, CancellationToken cancellationToken = default);

		/// <summary>
		/// Checks if an e-mail address is valid
		/// </summary>
		/// <param name="email">The e-mail address</param>
		/// <param name="cancellationToken">The cancellation token</param>
		/// <returns>True if it is a valid e-mail address</returns>
		Task<bool> IsValidEmailAddressAsync(string email, CancellationToken cancellationToken = default);
	}

	/// <summary>
	/// Applicant service implementation
	/// </summary>
	public class ApplicantValidatorCacheService : IApplicantValidatorCacheService
	{
		private readonly IHttpClientFactory _httpClientFactory;
		private readonly ILogger<ApplicantValidatorCacheService> _logger;

		private readonly string _countryValidationUrl;
		private readonly string _topLevelDomainsListUrl;
		private readonly LockProvider<string> _countryCacheLocker = new LockProvider<string>();
		private readonly ConcurrentDictionary<string, bool> _countryCache = new ConcurrentDictionary<string, bool>();
		private readonly SemaphoreSlim _topLevelDomainsListLocker = new SemaphoreSlim(1, 1);
		private readonly List<string> _topLevelDomainsList = new List<string>();

		/// <summary>
		/// Service constructor
		/// </summary>
		public ApplicantValidatorCacheService(ILogger<ApplicantValidatorCacheService> logger, IConfiguration configuration, IHttpClientFactory httpClientFactory)
		{
			_logger = logger;
			_httpClientFactory = httpClientFactory;

			_countryValidationUrl = configuration["ApplicantValidatorCacheService:CountryValidationUrl"];
			_topLevelDomainsListUrl = configuration["ApplicantValidatorCacheService:TopLevelDomainsListUrl"];
		}

		/// <summary>
		/// Checks if a country is present
		/// </summary>
		/// <param name="country">The country name</param>
		/// <param name="cancellationToken">The cancellation token</param>
		/// <returns>True if country is present</returns>
		public async Task<bool> IsValidCountryAsync(string country, CancellationToken cancellationToken = default)
		{
			bool ret = false;

			if (!string.IsNullOrEmpty(country))
			{
				// Lock for this country only
				await _countryCacheLocker.WaitAsync(country, cancellationToken);

				if (!cancellationToken.IsCancellationRequested)
				{
					if (!_countryCache.ContainsKey(country))
					{
						try
						{
							using var request = new HttpRequestMessage(HttpMethod.Get, string.Format(_countryValidationUrl, country));
							request.Headers.Accept.Clear();
							request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
							var httpClient = _httpClientFactory.CreateClient();
							var response = await httpClient.SendAsync(request, cancellationToken);
							if (response.IsSuccessStatusCode)
							{
								_countryCache.TryAdd(country, true);
								ret = true;
							}
						}
						catch (Exception ex)
						{
							_logger.LogError(ex, string.Format("IsValidCountryAsync({0}): {1}", country, ex.Message));
						}
					}
					else
					{
						ret = true;
					}
				}

				// Release the lock for this country
				_countryCacheLocker.Release(country);
			}

			return ret;
		}

		/// <summary>
		/// Checks if an e-mail address is valid
		/// </summary>
		/// <param name="email">The e-mail address</param>
		/// <param name="cancellationToken">The cancellation token</param>
		/// <returns>True if it is a valid e-mail address</returns>
		public async Task<bool> IsValidEmailAddressAsync(string email, CancellationToken cancellationToken = default)
		{
			if (!string.IsNullOrEmpty(email))
			{
				Match m = Regex.Match(email, @"^(.+)\@(.+)\.(.+)$");
				if (m.Groups.Count == 4)
				{
					// Lock for the first request
					await _topLevelDomainsListLocker.WaitAsync(cancellationToken);

					if (!cancellationToken.IsCancellationRequested)
					{
						if (_topLevelDomainsList.Count == 0)
						{
							try
							{
								using var request = new HttpRequestMessage(HttpMethod.Get, _topLevelDomainsListUrl);
								request.Headers.Accept.Clear();
								request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
								var httpClient = _httpClientFactory.CreateClient();
								var response = await httpClient.SendAsync(request, cancellationToken);
								if (response.IsSuccessStatusCode)
								{
									string[] list = (await response.Content.ReadAsStringAsync(cancellationToken)).Split('\n');
									foreach (string domain in list)
									{
										if (!domain.StartsWith("#"))
										{
											_topLevelDomainsList.Add(domain.ToLower());
										}
									}
								}
							}
							catch (Exception ex)
							{
								_logger.LogError(ex, string.Format("IsValidEmailAddressAsync({0}): {1}", email, ex.Message));
							}
						}
					}

					// Release the lock
					_topLevelDomainsListLocker.Release();

					return _topLevelDomainsList.Contains(m.Groups[3].Value.ToLower());
				}
			}

			return false;
		}
	}
}
