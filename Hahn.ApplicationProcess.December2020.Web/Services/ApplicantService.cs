﻿using Hahn.ApplicationProcess.December2020.Domain.Interfaces;
using Hahn.ApplicationProcess.December2020.Domain.Models;
using Hahn.ApplicationProcess.December2020.Web.Models;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Hahn.ApplicationProcess.December2020.Web.Services
{
	/// <summary>
	/// Applicant service interface
	/// </summary>
	public interface IApplicantService
	{
		/// <summary>
		/// Retrieves all applicants
		/// </summary>
		/// <param name="cancellationToken">The cancellation token</param>
		/// <returns>The applicants array</returns>
		Task<IEnumerable<Applicant>> GetApplicantsAsync(CancellationToken cancellationToken = default);

		/// <summary>
		/// Retrieves an applicant by the id
		/// </summary>
		/// <param name="id" example="1">The applicant id</param>
		/// <param name="cancellationToken">The cancellation token</param>
		/// <returns>The applicant</returns>
		Task<Applicant> GetApplicantAsync(int id, CancellationToken cancellationToken = default);

		/// <summary>
		/// Inserts a new applicant
		/// </summary>
		/// <param name="applicant">The applicant edit model</param>
		/// <param name="cancellationToken">The cancellation token</param>
		/// <returns>The applicant id</returns>
		Task<int> InsertApplicantAsync(ApplicantEditModel applicant, CancellationToken cancellationToken = default);

		/// <summary>
		/// Updates an applicant
		/// </summary>
		/// <param name="id" example="1">The applicant unique identifier</param>
		/// <param name="applicant">The applicant edit model</param>
		/// <param name="cancellationToken">The cancellation token</param>
		/// <returns>False if applicant not found</returns>
		Task<bool> UpdateApplicantAsync(int id, ApplicantEditModel applicant, CancellationToken cancellationToken = default);

		/// <summary>
		/// Deletes an applicant
		/// </summary>
		/// <param name="id" example="1">The applicant unique identifier</param>
		/// <param name="cancellationToken">The cancellation token</param>
		/// <returns>False if applicant not found</returns>
		Task<bool> DeleteApplicantAsync(int id, CancellationToken cancellationToken = default);
	}

	/// <summary>
	/// Applicant service implementation
	/// </summary>
	public class ApplicantService : IApplicantService
	{
		/// <summary>
		/// The Applicant repository
		/// </summary>
		public IApplicantRepository _applicantRepository;


		/// <summary>
		/// Applicant service constructor
		/// </summary>
		public ApplicantService(IApplicantRepository applicantRepository)
		{
			_applicantRepository = applicantRepository;
		}

		/// <summary>
		/// Retrieves all applicants
		/// </summary>
		/// <param name="cancellationToken">The cancellation token</param>
		/// <returns>The applicants list</returns>
		public async Task<IEnumerable<Applicant>> GetApplicantsAsync(CancellationToken cancellationToken = default)
		{
			return await _applicantRepository.GetApplicantsAsync(cancellationToken);
		}

		/// <summary>
		/// Retrieves an applicant by the id
		/// </summary>
		/// <param name="id" example="1">The applicant id</param>
		/// <param name="cancellationToken">The cancellation token</param>
		/// <returns>The applicant</returns>
		public async Task<Applicant> GetApplicantAsync(int id, CancellationToken cancellationToken = default)
		{
			return await _applicantRepository.GetApplicantAsync(id, cancellationToken);
		}

		/// <summary>
		/// Inserts a new applicant
		/// </summary>
		/// <param name="applicant">The applicant edit model</param>
		/// <param name="cancellationToken">The cancellation token</param>
		/// <returns>The applicant id</returns>
		public async Task<int> InsertApplicantAsync(ApplicantEditModel applicant, CancellationToken cancellationToken = default)
		{
			Applicant model = new Applicant
			{
				Name = applicant.Name,
				FamilyName = applicant.FamilyName,
				Address = applicant.Address,
				CountryOfOrigin = applicant.CountryOfOrigin,
				Age = applicant.Age,
				EMailAddress = applicant.EMailAddress,
				Hired = applicant.Hired
			};

			return await _applicantRepository.InsertApplicantAsync(model, cancellationToken);
		}

		/// <summary>
		/// Updates an applicant
		/// </summary>
		/// <param name="id" example="1">The applicant unique identifier</param>
		/// <param name="applicant">The applicant edit model</param>
		/// <param name="cancellationToken">The cancellation token</param>
		/// <returns>False if applicant not found</returns>
		public async Task<bool> UpdateApplicantAsync(int id, ApplicantEditModel applicant, CancellationToken cancellationToken = default)
		{
			Applicant model = new Applicant
			{
				Id = id,
				Name = applicant.Name,
				FamilyName = applicant.FamilyName,
				Address = applicant.Address,
				CountryOfOrigin = applicant.CountryOfOrigin,
				Age = applicant.Age,
				EMailAddress = applicant.EMailAddress,
				Hired = applicant.Hired
			};

			return await _applicantRepository.UpdateApplicantAsync(model, cancellationToken);
		}

		/// <summary>
		/// Deletes an applicant
		/// </summary>
		/// <param name="id" example="1">The applicant unique identifier</param>
		/// <param name="cancellationToken">The cancellation token</param>
		/// <returns>False if applicant not found</returns>
		public async Task<bool> DeleteApplicantAsync(int id, CancellationToken cancellationToken = default)
		{
			return await _applicantRepository.DeleteApplicantAsync(id, cancellationToken);
		}
	}
}
