﻿namespace Hahn.ApplicationProcess.December2020.Web.Models
{
	/// <summary>
	/// The applicant edit model
	/// </summary>
	public class ApplicantEditModel
	{
		/// <summary>
		/// The applicant first name
		/// </summary>
		/// <example>Massimo</example>
		public string Name { get; set; }

		/// <summary>
		/// The applicant last name
		/// </summary>
		/// <example>Superfustacchione</example>
		public string FamilyName { get; set; }

		/// <summary>
		/// The applicant address
		/// </summary>
		/// <example>via delle natiche bianche, 1</example>
		public string Address { get; set; }

		/// <summary>
		/// The applicant country code
		/// </summary>
		/// <example>ita</example>
		public string CountryOfOrigin { get; set; }

		/// <summary>
		/// The applicant e-mail address
		/// </summary>
		/// <example>massimo@superfustacchione.it</example>
		public string EMailAddress { get; set; }

		/// <summary>
		/// The applicant age
		/// </summary>
		/// <example>45</example>
		public int Age { get; set; }

		/// <summary>
		/// The applicant hired flag
		/// </summary>
		/// <example>true</example>
		public bool Hired { get; set; }
	}
}
