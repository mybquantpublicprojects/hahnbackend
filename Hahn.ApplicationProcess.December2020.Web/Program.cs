using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;

namespace Hahn.ApplicationProcess.December2020.Web
{
	/// <summary>
	/// Main program class
	/// </summary>
	public class Program
	{
		/// <summary>
		/// The main program function
		/// </summary>
		public static void Main(string[] args)
		{
			CreateHostBuilder(args).Build().Run();
		}

		/// <summary>
		/// The CreateHostBuilder function
		/// </summary>
		public static IHostBuilder CreateHostBuilder(string[] args) =>
			Host.CreateDefaultBuilder(args)
				.UseSerilog((ctx, config) =>
				{
					config
						.ReadFrom.Configuration(ctx.Configuration)
						.MinimumLevel.Information()
						.Enrich.FromLogContext()
						.WriteTo.Console();
				})
				.ConfigureWebHostDefaults(webBuilder =>
				{
					webBuilder.UseKestrel(options => options.AddServerHeader = false);
					webBuilder.ConfigureLogging(logging =>
					{
						 logging.AddFilter("Microsoft", LogLevel.Information);
						 logging.AddFilter("System", LogLevel.Error);
					});
					webBuilder.UseStartup<Startup>();
				});
	}
}
