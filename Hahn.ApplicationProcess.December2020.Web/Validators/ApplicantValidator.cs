﻿using FluentValidation;
using Hahn.ApplicationProcess.December2020.Web.Models;
using Hahn.ApplicationProcess.December2020.Web.Services;
using Microsoft.Extensions.Localization;

namespace Hahn.ApplicationProcess.December2020.Web.Validators
{
	/// <summary>
	/// Validator for the applicant edit model
	/// </summary>
	public class ApplicantValidator : AbstractValidator<ApplicantEditModel>
	{
		private readonly IApplicantValidatorCacheService _cacheService;

		/// <summary>
		/// The Validator constructor
		/// </summary>
		public ApplicantValidator(IStringLocalizer<ApplicantValidator> localizer, IApplicantValidatorCacheService cacheService)
		{
			_cacheService = cacheService;

			RuleFor(x => x.Name).MinimumLength(5);
			RuleFor(x => x.FamilyName).MinimumLength(5);
			RuleFor(x => x.Address).MinimumLength(10);
			RuleFor(x => x.EMailAddress).MustAsync(async (value, context) =>
			{
				return await _cacheService.IsValidEmailAddressAsync(value, context);
			}).WithMessage(x => localizer["EmailAddressValidationMessage"]);
			RuleFor(x => x.Age).InclusiveBetween(20, 60);
			RuleFor(x => x.Hired).NotEmpty().When(x => x.Hired);
			RuleFor(x => x.CountryOfOrigin).MustAsync(async (value, context) =>
			{
				return await _cacheService.IsValidCountryAsync(value?.ToLower(), context);
			}).WithMessage(x => localizer["CountryOfOriginValidationMessage"]);
		}
	}
}
