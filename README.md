# `hahn-ApplicationProcess-December2020-Backend`

This project is the backend side of Hanh Project.

## Setup

You need .NET 5.0 installed on your machine in order to run this application.

Use Visual Studio 2019 version 16.8 at least (previous versions do not include .NET 5.0)

Run `dotnet dev-certs https --trust` to install local https dev certificate, if needed

## Run dev app

To launch the application use Visual Studio or run `dotnet run --configuration Release` from command prompt inside `Hahn.ApplicationProcess.December2020.Web` folder.
