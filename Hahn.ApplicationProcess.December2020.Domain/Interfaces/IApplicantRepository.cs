﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Hahn.ApplicationProcess.December2020.Domain.Models;

namespace Hahn.ApplicationProcess.December2020.Domain.Interfaces
{
	/// <summary>
	/// The applicant repository interface
	/// </summary>
	public interface IApplicantRepository
	{
		/// <summary>
		/// Retrieves all applicants
		/// </summary>
		/// <param name="cancellationToken">The cancellation token</param>
		/// <returns>The applicants array</returns>
		Task<IEnumerable<Applicant>> GetApplicantsAsync(CancellationToken cancellationToken = default);

		/// <summary>
		/// Retrieves an applicant by the id
		/// </summary>
		/// <param name="id" example="1">The applicant id</param>
		/// <param name="cancellationToken">The cancellation token</param>
		/// <returns>The applicant</returns>
		Task<Applicant> GetApplicantAsync(int id, CancellationToken cancellationToken = default);

		/// <summary>
		/// Inserts a new applicant
		/// </summary>
		/// <param name="applicant">The applicant edit model</param>
		/// <param name="cancellationToken">The cancellation token</param>
		/// <returns>The applicant id</returns>
		Task<int> InsertApplicantAsync(Applicant applicant, CancellationToken cancellationToken = default);

		/// <summary>
		/// Updates an applicant
		/// </summary>
		/// <param name="applicant">The applicant edit model</param>
		/// <param name="cancellationToken">The cancellation token</param>
		/// <returns>False if applicant not found</returns>
		Task<bool> UpdateApplicantAsync(Applicant applicant, CancellationToken cancellationToken = default);

		/// <summary>
		/// Deletes an applicant
		/// </summary>
		/// <param name="id" example="1">The applicant id</param>
		/// <param name="cancellationToken">The cancellation token</param>
		/// <returns>False if applicant not found</returns>
		Task<bool> DeleteApplicantAsync(int id, CancellationToken cancellationToken = default);
	}
}
