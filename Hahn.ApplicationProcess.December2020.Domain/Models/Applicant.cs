﻿namespace Hahn.ApplicationProcess.December2020.Domain.Models
{
	/// <summary>
	/// The applicant model
	/// </summary>
	public class Applicant
	{
		/// <summary>
		/// The applicant unique identifier
		/// </summary>
		public int Id { get; set; }

		/// <summary>
		/// The applicant first name
		/// </summary>
		public string Name { get; set; }

		/// <summary>
		/// The applicant last name
		/// </summary>
		public string FamilyName { get; set; }

		/// <summary>
		/// The applicant address
		/// </summary>
		public string Address { get; set; }

		/// <summary>
		/// The applicant country code
		/// </summary>
		public string CountryOfOrigin { get; set; }

		/// <summary>
		/// The applicant e-mail address
		/// </summary>
		public string EMailAddress { get; set; }

		/// <summary>
		/// The applicant age
		/// </summary>
		public int Age { get; set; }

		/// <summary>
		/// The applicant hired flag
		/// </summary>
		public bool Hired { get; set; }
	}
}